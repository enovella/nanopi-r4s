# Firmware

## OpenWRT

- FriendlyWRT - https://drive.google.com/drive/folders/1C0-QsuJRXYfRpcHEU6lCM_lV4PT1z6t-
  - wiki: https://wiki.friendlyarm.com/wiki/index.php/NanoPi_R4S#First_boot
- QuintusLab - https://github.com/quintus-lab/OpenWRT-R2S-R4S


## OpnSense

- https://people.freebsd.org/~ganbold/OPNsense-20.7-OpenSSL-aarch64-nanopi-r4s-20201215.img.xz
- https://mirror.fkardame.com/Linux/Images/FriendlyArm/NanoPi%20R4s/

# Setup

## Clean up and basics install
```sh
opkg update
opkg remove luci-i18n-*
# opkg list-upgradable | cut -f 1 -d ' ' | xargs opkg upgrade
opkg install htop
```

## Static IP

- Static ip address on lan:
```sh
  # vim /etc/config/network
  config interface 'lan'
          option type 'bridge'
          option ifname 'eth1'
          option proto 'static'
          option ipaddr '10.0.0.1'
          option netmask '255.255.255.0'
          option ip6assign '60'
          option ipv6 'off'
```
_Notice this could be done also via uci/commit commands_

- Disable IPv6:
```sh
uci set 'network.lan.ipv6=off'
uci set 'network.wan.ipv6=off'
uci set 'dhcp.lan.dhcpv6=disabled'
/etc/init.d/odhcpd disable
uci commit
```

## Security basics

- Change password (System > Administration > Router Password)
- Only allow ssh access from lan (System > Administration > SSH Access)
- Only allow local devices to access luci:

```sh
    # vim /etc/config/uhttpd
    # HTTP listen addresses, multiple allowed
    list listen_http 10.0.0.1:80
    #list listen_http	[::]:80
```

`/etc/init.d/uhttpd restart`

## Add SSH Key-based Authentication

Choose one way:

- Using `ssh-copy-id`
```sh
ssh-copy-id root@10.0.0.1
```

- Copying your RSA pub key into dropbear authorized SSH keys:
```sh
ssh root@10.0.0.1 "mkdir -p ~/.ssh; tee -a ~/.ssh/authorized_keys" < ~/.ssh/id_rsa.pub
```

- Manage Dropbear keys using web interface:

Navigate to `LuCI` → `System` → `Administration` → `SSH-Keys`.
Copy-paste your public key and click the Add key button.

# Backup & Restore

# Boot

# Bridge Mode

**TEST WITHOUT VPN ON**

```sh
#/etc/config/network
config interface 'wan'
        option ifname 'vlan20'
        option proto 'dhcp'
        option ipv6 'off'
```

```sh
config device
        option type '8021q'
        option ifname 'eth0'
        option vid '20'
        option name 'vlan20'
```

# System

## Scheduled Tasks

Auto-update adblock & banIP lists every day at 04am:
```sh
# crontab -e
0 4 * * * /etc/init.d/adblock restart
0 4 * * * /etc/init.d/banip reload
```

# Services

## WireGuard

- Dependencies:
```sh
opkg update
opkg install --force-depends wireguard wireguard-tools luci-app-wireguard luci-proto-wireguard qrencode
```

### Server setup

TODO

Enable logs in `dmesg`:

```
echo 'module wireguard +p' | sudo tee /sys/kernel/debug/dynamic_debug/control
```

### Create peers

- Copy the script `scripts/create-peer.sh` into the nano-pi and update it with your configuration. It will update both `/etc/config/network` with the generated peer and provide its config (JSON and QR).

## Adblock

- OpenWRT doc: https://openwrt.org/docs/guide-user/services/ad-blocking

- List blacklist domain lists:
```sh
root@FriendlyWrt:~# /etc/init.d/adblock list
::: Available adblock sources
:::
    Name                 Enabled   Size   Focus                Info URL
    -------------------------------------------------------------------
  + adaway               x         S      mobile               https://github.com/AdAway/adaway.github.io
  + adguard              x         L      general              https://adguard.com
  + android_tracking               S      tracking             https://github.com/Perflyst/PiHoleBlocklist
  + andryou                        L      compilation          https://gitlab.com/andryou/block/-/blob/master/rea
  + anti_ad                        L      compilation          https://github.com/privacy-protection-tools/anti-A
  + anudeep                        M      compilation          https://github.com/anudeepND/blacklist
  + bitcoin                        S      mining               https://github.com/hoshsadiq/adblock-nocoin-list
  + disconnect           x         S      general              https://disconnect.me
  + stevenblack          x         L      compilation          https://github.com/StevenBlack/hosts
  ...
```

- Installation and configuration to enable logs in LuCI:
```sh
opkg update
opkg install adblock luci-app-adblock libustream-mbedtls tcpdump-mini
uci set adblock.global.adb_enabled="1"
uci set adblock.global.adb_debug="1"
uci set adblock.global.adb_report="1"
uci set adblock.global.adb_backupdir="/etc/adblock"
uci commit adblock
/etc/init.d/adblock restart
```

- Configuration file
```sh
root@FriendlyWrt:~# cat /etc/config/adblock

config adblock 'global'
	option adb_enabled '1'
	option adb_forcedns '0'
	option adb_safesearch '0'
	option adb_dnsfilereset '0'
	option adb_mail '0'
	option adb_backup '1'
	option adb_maxqueue '4'
	option adb_dns 'dnsmasq'
	option adb_fetchutil 'aria2c'
	option adb_debug '1'
	option adb_report '1'
	option adb_backupdir '/etc/adblock'
	option adb_repiface 'br-lan'
	list adb_sources 'adaway'
	list adb_sources 'adguard'
	list adb_sources 'android_tracking'
	list adb_sources 'disconnect'
	list adb_sources 'reg_es'
	list adb_sources 'smarttv_tracking'
	list adb_sources 'stevenblack'
	list adb_sources 'yoyo'
```

- Note: If DNS report does not show anything, make sure you didn't install OpenSSL
```sh
opkg remove libustream-openssl20150806
```

- Note: If the lists are breaking your DHCP, restart it with : `service dnsmasq restart`
or add this line in your `/etc/rc.local`: `/etc/init.d/dnsmasq restart &`


## DDNS

- Installation:
```sh
opkg update
opkg install luci-app-ddns ddns-scripts
```

- Configuration
```sh
root@FriendlyWrt:~# cat /etc/config/ddns

config ddns 'global'
	option ddns_dateformat '%F %R'
	option ddns_loglines '250'
	option upd_privateip '0'

config service 'lamansiondedudu'
	option update_url 'https://freedns.afraid.org/dynamic/update.php?{THE_TOKEN}='
	option lookup_host 'lamansiondedudu.mooo.com'
	option domain 'lamansiondedudu.mooo.com'
	option username 'dummy_not_used'
	option password 'dummy_not_used'
	option interface 'wan'
	option ip_source 'network'
	option ip_network 'wan'
	option enabled '1'

config service 'myddns_ipv6'
	option update_url 'http://[USERNAME]:[PASSWORD]@your.provider.net/nic/update?hostname=[DOMAIN]&myip=[IP]'
	option lookup_host 'yourhost.example.com'
	option domain 'yourhost.example.com'
	option username 'your_username'
	option password 'your_password'
	option use_ipv6 '1'
	option interface 'wan6'
	option ip_source 'network'
	option ip_network 'wan6'
	option enabled '0'
```

## Transmission

- Installation:
```sh
opkg update
opkg install transmission-daemon-openssl
opkg install transmission-cli-openssl
opkg install transmission-web
opkg install transmission-remote-openssl

uci set transmission.@transmission[0].enabled="1"
uci commit transmission
/etc/init.d/transmission restart
/etc/init.d/transmission enable
```

- Web access:

`http://10.0.0.1:9091/transmission/web/`

- Store downloads in mounted usb (documented bellow):

```
chown -R transmission:transmission /mnt/sda
```

## Qbittorernt

- Install via docker:
https://github.com/linuxserver/docker-qbittorrent

```sh
mkdir -p /qbittorrent/config

docker run -d \
  --name=qbittorrent \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Europe/Madrid \
  -e WEBUI_PORT=8080 \
  -p 6881:6881 \
  -p 6881:6881/udp \
  -p 8080:8080 \
  -v /qbittorrent/config:/config \
  -v /mnt/sda:/downloads \
  --restart unless-stopped \
  ghcr.io/linuxserver/qbittorrent
```

## Play downloaded movies with Jellyfin

https://jellyfin.org/docs/general/administration/installing.html#docker

```sh
mkdir -p /jellyfin/config
mkdir -p /jellyfin/cache
docker run -d \
 --name jellyfin \
 --net=host \
 --mount type=bind,source=/jellyfin/config,target=/config \
 --mount type=bind,source=/jellyfin/cache,target=/cache \
 --mount type=bind,source=/mnt/sda,target=/media \
 --restart=unless-stopped \
 jellyfin/jellyfin:10.7.0-arm64
```

TODO:
- change default transmission configs


# Network

##  SQM QoS - Smart Queue Management
- [x] Enable this SQM instance. (eth1 (lan))


# System

## Mount usb

https://openwrt.org/docs/guide-user/storage/usb-drives-quickstart

```
opkg update && opkg install block-mount e2fsprogs kmod-fs-ext4 kmod-usb-storage kmod-usb2 kmod-usb3
```

```
ls -la /dev/sd*
brw-------    1 root     root        8,   0 Mar 19 09:29 /dev/sda
```

Check config:

```
block detect
```

Commit to fstab:

```
block detect | uci import fstab
```

Mount on startup (notice 0=sda, 1=sdb):

```
uci set fstab.@mount[0].enabled='1' && uci set fstab.@global[0].anon_mount='1' && uci commit fstab
```

Chec System > Mount Points after reboot.

## Scheduled tasks

```
PATH=/usr/sbin:/usr/bin:/sbin:/bin

3,8,13,18,23,28,33,38,43,48,53,58 * * * * sleep 53 ; wget --no-check-certificate -O - https://freedns.afraid.org/dynamic/update.php?<FREE_DNS_API_TOKEN> >> /tmp/freedns_cron.log 2>&1 &
```

