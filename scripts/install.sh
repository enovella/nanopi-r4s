opkg update
opkg remove luci-i18n-*
# opkg list-upgradable | cut -f 1 -d ' ' | xargs opkg upgrade
opkg install --force-depends wireguard wireguard-tools luci-app-wireguard luci-proto-wireguard qrencode
opkg install htop

opkg install adblock luci-app-adblock libustream-mbedtls tcpdump-mini
uci set adblock.global.adb_enabled="1"
uci set adblock.global.adb_debug="1"
uci set adblock.global.adb_report="1"
uci set adblock.global.adb_backupdir="/etc/adblock"
uci commit adblock
/etc/init.d/adblock restart

opkg install luci-app-ddns ddns-scripts