#!/bin/sh

PEER_USER=$1
PEER_IP=$2
DEVICE=$3

if [ -z $PEER_USER ] || [ -z $PEER_IP ]; then
  echo "[!] Usage: <peer-user> <peer-ip> <ios?>"
  exit 1
fi

if echo $PEER_USER | grep -Eq '[^a-zA-Z0-9]'; then
  echo "[!] Peer user cannot contain symbols"
  exit 1
fi

# Configuration vars
WG_IF="wg0"
SERVER_PUB="$(cat /etc/wireguard/wgserver.pub)"
SERVER_IP="10.1.0.1"
SERVER_ENDPOINT="lamansiondedudu.mooo.com:10001"
SERVER_CONF_FILE="/etc/wireguard/wg0.conf"
DNS_IPS="10.1.0.1"
KEYS_DIR="/etc/wireguard/keys"

case $DEVICE in
  ios)
    echo "[+] Using iOS allowed ips"
    PEER_ALLOWED_IPS="1.0.0.0/8, 2.0.0.0/8, 3.0.0.0/8, 4.0.0.0/6, 8.0.0.0/7, 11.0.0.0/8, 12.0.0.0/6, 16.0.0.0/4, 32.0.0.0/3, 64.0.0.0/2, 128.0.0.0/3, 160.0.0.0/5, 168.0.0.0/6, 172.0.0.0/12, 172.32.0.0/11, 172.64.0.0/10, 172.128.0.0/9, 173.0.0.0/8, 174.0.0.0/7, 176.0.0.0/4, 192.0.0.0/9, 192.128.0.0/11, 192.160.0.0/13, 192.169.0.0/16, 192.170.0.0/15, 192.172.0.0/14, 192.176.0.0/12, 192.192.0.0/10, 193.0.0.0/8, 194.0.0.0/7, 196.0.0.0/6, 200.0.0.0/5, 208.0.0.0/4, ${SERVER_IP}/32"
    ;;
  *)
    echo "[+] Using default allowed ips"
    PEER_ALLOWED_IPS="0.0.0.0/0"
    ;;
esac

mkdir -p $KEYS_DIR

KEY_FILE="${KEYS_DIR}/${PEER_USER}.key"
PUB_FILE="${KEYS_DIR}/${PEER_USER}.pub"
PSK_FILE="${KEYS_DIR}/${PEER_USER}.psk"
CONF_FILE="${KEYS_DIR}/${PEER_USER}.conf"

# Generate client keys
umask go=
wg genkey | tee $KEY_FILE | wg pubkey > $PUB_FILE
wg genpsk > $PSK_FILE

# Peer private key
PEER_KEY="$(cat $KEY_FILE)"
PEER_PUB="$(cat $PUB_FILE)"
PEER_PSK="$(cat $PSK_FILE)"

# Peer conf file
cat << EOF > $CONF_FILE
[Interface]
PrivateKey = ${PEER_KEY}
Address = ${PEER_IP}/24
DNS = ${DNS_IPS}

[Peer]
AllowedIPs = ${PEER_ALLOWED_IPS}
Endpoint = ${SERVER_ENDPOINT}
PreSharedKey = ${PEER_PSK}
PublicKey = ${SERVER_PUB}
PersistentKeepalive = 25
EOF

echo "[*] Conf file can be found in ${CONF_FILE}"

# Server config
uci set network.${PEER_USER}="wireguard_${WG_IF}"
uci set network.${PEER_USER}.description="${PEER_USER}"
uci set network.${PEER_USER}.public_key="${PEER_PUB}"
uci set network.${PEER_USER}.preshared_key="${PEER_PSK}"
uci set network.${PEER_USER}.route_allowed_ips="1"
uci set network.${PEER_USER}.persistent_keepalive="25"
uci add_list network.${PEER_USER}.allowed_ips="${PEER_IP}/32"
uci add_list network.${PEER_USER}.allowed_ips="::/0"
uci commit network

echo "[*] Adding peer pub key in server conf"
cat << EOF >> $SERVER_CONF_FILE

[Peer] - ${PEER_USER}
PublicKey = ${PEER_PUB}
AllowedIPs = ${PEER_IP}/32
EOF

echo "[*] Restarting network..."
/etc/init.d/network restart

#Print QR code
qrencode -t UTF8 < "${CONF_FILE}"